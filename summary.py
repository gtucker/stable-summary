#!/usr/bin/env python3
#
# Copyright (C) 2022 Collabora Limited
# Author: Guillaume Tucker <guillaume.tucker@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import argparse
from datetime import datetime, timezone
import json
import sys
import urllib

from jinja2 import Environment, FileSystemLoader
import pymongo


def get_revision(database, rev_opt):
    build = database['build'].find_one(rev_opt)
    if not build:
        return {}
    return {
        'sha1': build['git_commit'],
        'tag': build['git_describe_v'],
    }


def get_builds_summary(args, database, rev_opt):
    builds = database['build'].find(rev_opt)
    data = {}
    for build in builds:
        arch = data.setdefault(build['arch'], {})
        compiler = arch.setdefault(build['build_environment'], {})
        url = urllib.parse.urljoin(
            args.base_url, '/'.join(['build/id', str(build['_id'])])
        )
        compiler[build['defconfig_full']] = {
            'url': url,
            'status': 'warn' if build['warnings'] else build['status'].lower(),
            'attributes': {
                'errors': build['errors'],
                'warnings': build['warnings'],
            },
        }
    return data


def cmd_get_data(args):
    rev_args = (args.git_sha1, args.git_tag)
    if all(rev_args) or not any(rev_args):
        print("Either --git-sha1 or --git-tag required")
        return False
    if args.git_sha1:
        rev_opt = {'git_commit': args.git_sha1}
    elif args.git_tag:
        rev_opt = {'kernel': args.git_tag}
    client = pymongo.MongoClient()
    database = client[args.db]
    data = {
        'revision': get_revision(database, rev_opt),
        'date': {
            'iso': datetime.now(tz=timezone.utc).isoformat(),
        },
        'results': {
            'builds': get_builds_summary(args, database, rev_opt),
        },
    }
    if args.json == '-':
        print(json.dumps(data, indent='  '))
    else:
        file_name = args.output or '.'.join((data['revision']['tag'], 'json'))
        print(f"Saving data in {file_name}")
        with open(file_name, 'w') as json_file:
            json.dump(data, json_file, indent='  ')
    return True


def cmd_render(args):
    jinja2_env = Environment(loader=FileSystemLoader('.'))
    template = jinja2_env.get_template(args.template)
    with open(args.json) as input_file:
        data = json.load(input_file)
    summary = template.render(data)
    if args.output == '-':
        print(summary)
    else:
        file_name = args.output or '.'.join((data['revision']['tag'], 'md'))
        print(f"Saving summary in {file_name}")
        with open(file_name, 'w') as f:
            f.write(summary)
    return True


def main(args):
    parser = argparse.ArgumentParser("Stable summary")
    parser.add_argument('--db', default='kernelci',
                        help="Name of the Mongo database")
    parser.add_argument('--git-sha1',
                        help="Git SHA1 to use for the summary")
    parser.add_argument('--git-tag',
                        help="Git tag to use for the summary")
    parser.add_argument('--base-url', default='https://linux.kernelci.org',
                        help="Base URL for the detailed results")
    parser.add_argument('--json',
                        help="Path to the JSON file to store the data")
    parser.add_argument('--template', default='summary.md.jinja2',
                        help="Path to the summary template file")
    parser.add_argument('--output',
                        help="Path to the rendered summary file")
    parser.add_argument('command', choices=['get_data', 'render'])
    args = parser.parse_args(args)
    fname = '_'.join(['cmd', args.command])
    globals().get(fname)(args)


if __name__ == '__main__':
    main(sys.argv[1:])
