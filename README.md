# Linux Kernel Stable Release Test Summary Generator

This tool can retrieve test results from a KernelCI Mongo database to produce a
summary in Markdown format which can then be included in a static website.

For example, one page was generated for
[v5.10.123](https://static.staging.kernelci.org/docs/tests/kernel-releases/v5.10.123/) with a summary of all the kernel builds.

> **WARNING** This is still experimental and in early development stages.

## Prerequisites

To install the Python dependencies:

```
$ sudo apt install python3 python3-pip
$ pip install -y requirements-dev.txt
```

You'll also need a local Mongo DB with the KernelCI test data.  Please get in
touch by email on [kernelci@groups.io](mailto:kernelci@groups.io) or IRC
`#kernelci` on libera.chat for more details about that.

Alternatively, you may produce the JSON data structure by other means and then
render it with this script into a Markdown summary.

## Sample commands

To produce a JSON file with the summary data:

```
$ ./summary.py get_data --git-tag=v5.10.123 --db=kernelci-stable-2022
Saving summary in v5.10.123.json
```

Then to generate a Markdown summary using the JSON data:

```
$ ./summary.py render --db=kernelci-stable-2022 --json=v5.10.123.json
Saving summary in v5.10.123.md
```
